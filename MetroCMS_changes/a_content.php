<?php

class AContent
{

    function index()
    {
        global $_isAContent;
        $_isAContent = true;
        if (empty($_REQUEST['address'])) {
            $_REQUEST['address'] = '/';
        }
        $_REQUEST['address'] = trim($_REQUEST['address']);
        if ($_REQUEST['address'][0] == '/')
            $_REQUEST['address'] = substr($_REQUEST['address'], 1);
        $_GET['q'] = $_REQUEST['q'] = $_REQUEST['address'];
        $fc = new \FrontController('start');
        $fc->addModule('common');
        $fc->addModule('cms');

        $fc->dispatch(false);
        $c = ob_get_contents();
        ob_end_clean();
        $data = array();
        if (isset($fc->ctrl->_titlesData))
            $data = $fc->ctrl->_titlesData;
        $data['controller'] = $fc->controller;
        $data['action'] = $fc->action;
        $ajaxReplacements = [];

        if (!empty($data['ajaxReplacements'])) {
            $ajaxReplacements = $data['ajaxReplacements'];
            unset($data['ajaxReplacements']);
        }
        $ret = ['replacements' => $ajaxReplacements, 'html' => $c, 'data' => $data];


        return $ret;
    }

}

