import {AjaxLoader} from './src/AjaxLoader';
import {MetroAjaxLoader} from "./src/MetroAjaxLoader";

export {AjaxLoader, MetroAjaxLoader};
export default AjaxLoader;