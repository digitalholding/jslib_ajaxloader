import $ from 'jquery';
import 'url-polyfill';

export const AjaxLoader = {
    _config: {
        saveHistory: true,
        contentContainerSelector: '.contentContainer',
        linkSelector: '.ajaxLoader',
        legacyEnabled: false,
        ignoredLinks: []
    },

    _events: {
        loaded: [],
        beforeLoaded: [],
        progress: [],
        beforeReplaceHtml: [],
        hashChanged: [],
        error: []
    },

    init: function (config) {
        this._assetsVersion = window.assetsVersion || null;
        this.updateConfig(config);
        this._initListeners();
        this._tryInitLegacy();
    },

    /**
     * updates config
     * NOT REPLACE
     * @param config
     */
    updateConfig: function (config) {
        for (let name in config) {
            this._config[name] = config[name];
        }
    },

    _initListeners: function () {
        $(document).on('click.AjaxLoader', this._config.linkSelector, event => this._tryGo(new URL(event.currentTarget.href || document.location)));
        $(window).on('popstate.AjaxLoader', e => this._tryGo(document.location));
    },

    _tryInitLegacy: function () {
        if (this._config.legacyEnabled) {
            $(document).on('click.AjaxLoader', '.fire-clickAction', event => this._tryGo(new URL(event.currentTarget.href || document.location)));
        }
    },

    _tryGo: function (url) {
        if (this._canGo(url)) {
            this.go(url);
            return false;
        }

        return true;
    },

    _canGo: function (url) {
        if (url.hostname !== document.location.hostname) {
            return false;
        }

        for (let i = 0; i < this._config.ignoredLinks.length; i++) {
            let ignoredLink = this._config.ignoredLinks[i];

            if (ignoredLink instanceof RegExp && ignoredLink.test(url.pathname)) {
                return false;
            } else if (ignoredLink === url.pathname) {
                return false;
            }
        }

        return true;
    },

    go: function (address, config = {}) {
        if (!window.Promise) {
            document.location = address;
            return false;
        }

        for (let name in this._config) {
            config[name] = this._config[name];
        }
        return new Promise((resolve, reject) => {
            let url = (address instanceof URL) ? address : new URL(address, document.location);

            if (!this._canGo(url)) {
                reject();
                return;
            }

            this._computeContentChange(url, config)
                .then(() => {
                    this._updateScroll(url);
                    this._updateUrl(url);
                    resolve();
                })
                .catch(d => {
                    this._callEvents(this._events.error, [url, d]);
                    reject(d);
                })
        });
    },

    _computeContentChange: function (url, config) {
        return new Promise((resolve, reject) => {
            if (this._shouldGetDataFromServer(url)) {
                this._callEvents(this._events.beforeLoaded, [url, config]);
                $(document).trigger('ajaxLoaderBeforeLoaded', url);
                let startDate = new Date();
                this._getDataFromServer(url)
                    .then(d => {
                        this._computeVersionChange(d, url);
                        return this._computeNewData(d, startDate)
                    })
                    .then(() => {
                        resolve();
                    })
                    .catch(d => {
                        reject(d);
                    })
            } else {
                resolve();
            }
        });
    },

    _getHash(url) {
        let hash = url.hash;
        if (hash !== '' || url.href.substr(-1) === '#') {
            return hash;
        }

        return null;
    },

    _callEvents: function (eventListeners, args) {
        let callbackBreak = [];
        for (let i = 0; i < eventListeners.length; i++) {
            if (eventListeners[i].apply(window, args) === false) {
                callbackBreak.push(eventListeners[i])
            }
        }

        if (callbackBreak.length) {
            throw {type: 'eventBreak', callbacks: callbackBreak};
        }
    },

    _shouldGetDataFromServer(url) {
        let hash = this._getHash(url);
        return url.pathname !== document.location.pathname || url.hostname !== document.location.hostname || url.protocol !== document.location.protocol || hash === null;
    },

    _getDataFromServer(url) {
        this._callEvents(this._events.progress, [0]);
        return new Promise((resolve, reject) => {
            return this._ajaxRequest(url)
                .done(d => {
                    this._callEvents(this._events.progress, [100]);
                    resolve(d);
                })
                .fail(d => reject(d))
        })
    },

    _ajaxXhrProgress: function () {
        let xhr = new window.XMLHttpRequest();
        xhr.addEventListener("progress", (evt) => {
            if (evt.lengthComputable) {
                let percentComplete = evt.loaded / evt.total;
                this._callEvents(this._events.progress, [percentComplete]);
            }
        }, false);
        return xhr;
    },

    _ajaxRequest(requestUrl) {
        return $.ajax({
            xhr: this._ajaxXhrProgress.bind(this),
            url: requestUrl,
            data: {ajaxLoader: true},
            type: 'GET'
        });
    },

    _computeNewData: function (data, startDate) {
        return new Promise((resolve, reject) => {
            //redirect
            if (data.location) {
                this.go(data.location);
                resolve();
                return;
            }

            this._callEvents(this._events.beforeReplaceHtml, [data]);
            $(document).trigger('ajaxLoaderBeforeReplaceHtml', data);
            this._waitTo(this._config.minimalWait ? startDate * 1 + this._config.minimalWait : 0).then(() => {
                this._updateHeaderData(data);
                return this._updateBody(data)
            }).then(() => {
                this._callEvents(this._events.loaded, [data]);
                $(document).trigger('ajaxLoaderLoaded', data);
                resolve();
            }).catch(d => reject(d))

        })
    },
    _waitTo(date) {
        return new Promise((resolve, reject) => {
            let current = new Date() * 1;
            if (date <= current)
                resolve();
            else
                setTimeout(resolve, date - current);
        });
    },
    _computeVersionChange(data, url) {
        if (data.version && data.version.assets && data.version.assets !== this._assetsVersion) {
            document.location = url.href;
        }
    },

    _updateHeaderData: function (data) {
        if (data.data && data.data.title) {
            document.title = data.data.title
        }
    },

    _updateBody: function (data) {
        return new Promise((resolve, reject) => {
            $(this._config.contentContainerSelector).html(data.html);

            if (data.replacements && !Array.isArray(data.replacements)) {
                for (let selector in data.replacements) {
                    $(selector).html(data.replacements[selector]);//menu i inne takie, które w content się nie meiszczą
                }
            }
            resolve();
        });
    },

    _updateScroll: function (url) {
        let hash = this._getHash(url);
        if (hash) {
            $(window).scrollTop($(hash).offset().top);
        } else if (hash === null || (hash !== '' && url.href !== document.location.href)) {
            $(window).scrollTop(0);
        }
    },

    _updateUrl: function (url) {
        if (url.href !== document.location.href) {
            url.protocol = (new URL(document.URL)).protocol;
            window.history.pushState('', 'Title', url);
        }
    },

    onLoaded: function (callback) {
        this._events.loaded.push(callback);
    },

    onBeforeLoaded: function (callback) {
        this._events.beforeLoaded.push(callback);
    },

    onBeforeReplaceHtml: function (callback) {
        this._events.beforeReplaceHtml.push(callback);
    },

    onProgress: function (callback) {
        this._events.progress.push(callback);
    },

    onError: function (callback) {
        this._events.error.push(callback);
    }

};


