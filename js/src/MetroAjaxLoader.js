import $ from 'jquery';
import {AjaxLoader} from './AjaxLoader';

export const MetroAjaxLoader = Object.create(AjaxLoader);
MetroAjaxLoader._ajaxRequest = function (url) {
    return $.ajax({
        xhr: this._ajaxXhrProgress.bind(this),
        url: '/ajax/content',
        type: 'POST',
        data: {address: url.pathname + url.search}
    });
};

