# Simple init
Standardowo wlacza onclick na elementy z klasa "ajaxLoader". Mozna to nadpisac konfigiem

```javascript
import AjaxLoader from 'ajaxLoader';
AjaxLoader.init();
```
``` html
<a href="/" class="ajaxLoader">link</a>
```

# Symfony debug init
```javascript
import AjaxLoader from 'ajaxLoader';
AjaxLoader.init({ignoredLinks:[/\/_profiler\/*/]});
```

# MetroCMS version 
```javascript
import {MetroAjaxLoder} from 'ajaxLoader';
AjaxLoader.init();
```
Trzeba zmienic a_content.php na [a relative link](MetroCMS_changes/a_content.php)


# API
 - init(config): void;
 - updateConfig(config): void;
 - go(address): Promise;
 
 ## eventy
 W Eventach mozna zwrocic ***false***, co poskutkuje przerwaniem dzialania skryptu i wlaczy event *onError*
 - onBeforeLoaded(callback(url)): void; //przed polazeniem sie z serwerem
 - onBeforeReplaceHtml(callback(data)): void; //przed podmiana html'a
 - onProgress(callback(progress)): void; // w trakcie pobierania danych z serwera
 - onLoaded(callback(data)): void; // po podmianie html'a
 - onError(callback(data)): void; // jesli wystapil jakikolwiek blad

# Default config
```javascript
 let config = {
        saveHistory: true,
        contentContainerSelector: '.contentContainer',
        linkSelector: '.ajaxLoader',
        legacyEnabled: false,
        ignoredLinks: []
    }
```